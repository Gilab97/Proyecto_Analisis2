export interface IUser{
    idUsuario: number;
    nombres: string;
    apellidos: string;
    fecha_nacimiento: Date;
    correo_electronico: string;
    contrasena: string;
    descripcion: string;
    tipo_usuario: number;
    id_colonia: number;
}